FROM node:8.2.0-alpine

RUN mkdir /bot
WORKDIR /bot

COPY . /bot

RUN yarn install
RUN yarn build
CMD yarn docker:start