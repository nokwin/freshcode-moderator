import 'babel-polyfill';

import Telegraf, { log } from 'telegraf';
import { config as dotenv } from 'dotenv';
import rp from 'request-promise';
import _ from 'lodash';

dotenv();

const bot = new Telegraf(process.env.BOT_TOKEN);

bot.use(log());

bot.use((ctx, next) => {
	const { update: { edited_message: message }} = ctx;

	if(message && message.text.match(/(.+)\/tr$/)) {
		return transliterateMessage(ctx);
	}

	next();
});


const transliterateMessage = (ctx) => {
	const changeLetter = (letter) => {
		const index = _.findIndex(en, e => e === letter);

		return index === -1 ? letter : ru[index];
	};

	const { update: { edited_message: message }, reply } = ctx;
	const { chat: { id: chatId }, from: { username, first_name: firstName, last_name: lastName } } = message;

	const [full, text] = message.text.match(/(.+)\/tr$/);
	const en = 'qwertyuiop[]asdfghjkl;\'\\zxcvbnm,.QWERTYUIOP{}ASDFGHJKL:"|ZXCVBNM<>';
	const ru = 'йцукенгшщзхъфывапролджэёячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ';

	const splittedText = text.split(/\s/);

	const trans = _.map(
		splittedText,
		chunk => _.map([...chunk], t => chunk[0] === '@' ? t : changeLetter(t)).join('').trim()
		)
		.join(' ')
		.trim();

	reply(`Тот пиздюк ${username ? `@${username}` : `${firstName ? firstName : ''} ${lastName ? lastName : ''}`} сказал: ${trans}`);
};

const generateCaption = (from, chatId, text, url, imgurData) => {
	const { username, first_name: firstName, last_name: lastName } = from;

	let caption;
	let additional = '';

	if(text.trim() !== url) {
		text = text.replace(url, '${тут была ссылка}');

		additional = `Текст: ${text}`;
	}
	caption = `Отправитель:${username ? ` @${username},` : ''} ${firstName ? firstName : ''} ${lastName ? lastName : ''}\n${additional}`;

	if(imgurData && imgurData.title) caption += `\n${imgurData.title}`;

	return { caption };
};

const catchError = async (e, telegram, url) => {
	try {
		await telegram.sendMessage(process.env.TEST_CHAT, e);
		await telegram.sendMessage(process.env.TEST_CHAT, url);
	} catch(e) {
		console.log('Ну бля :(');
	}
};

const sendMessage = async (ctx) => {
	const {
		match: [full, url],
		replyWithDocument,
		replyWithPhoto,
		telegram,
		update: { message: { message_id: messageId, chat: { id: chatId }, from, text } },
		imgurData,
		isPhoto = false
	} = ctx;

	if(isPhoto) {
		try {
			await replyWithPhoto(url, generateCaption(from, chatId, text, url, imgurData));

			telegram.deleteMessage(chatId, messageId);
		} catch(e) {
			return catchError(e, telegram, url);
		}
	} else {
		try {
			await replyWithDocument(
				url.endsWith('.mp4') ? url : url.replace(/\.gifv?$/g, '.mp4'),
				generateCaption(from, chatId, text, url, imgurData)
			);

			telegram.deleteMessage(chatId, messageId);
		} catch(e) {
			try {
				await replyWithDocument(url, generateCaption(from, chatId, text, url, imgurData));

				telegram.deleteMessage(chatId, messageId);
			} catch(e) {
				return catchError(e, telegram, url);
			}
		}
	}
};

const imgurWrapper = async (ctx, next) => {
	const { match: [full, url], update: { message: { chat: { id: chatId } } } } = ctx;

	try {
		const [full, imageHash] = url.match(/https?:\/\/i\.imgur\.com\/(.+)\.(gifv?|mp4)/);

		const answer = await rp({
			url: `https://api.imgur.com/3/image/${imageHash}`,
			headers: { 'Authorization': `Client-ID ${process.env.IMGUR_CLIENT_ID}` },
			json: true
		});

		ctx.imgurData = answer.data;

		next();
	} catch(e) {
		next();
	}
};

const sendAngryMessage = ({ replyWithSticker }) => {
	const stickers = ['CAADAgADoQEAAmX_kgpHUNFlfFP7UgI', 'CAADAgADmwEAAmX_kgoH1FNzYkxGhwI',
		'CAADAgADnAEAAmX_kgr_4VqrZYvdlgI', 'CAADAgADnQEAAmX_kgq5XafocfM6RAI', 'CAADAgADngEAAmX_kgqSsJCtbPex6gI',
		'CAADAgADnwEAAmX_kgo0448twJxMbQI', 'CAADAgADoAEAAmX_kgqT2CudhzbNxQI', 'CAADAgADkwIAAmX_kgrAaVbuBrlzmAI',
		'CAADAgADlAIAAmX_kgpZNyH6jUJtiQI'];

	replyWithSticker(stickers[_.random(0, stickers.length - 1)]);
};

const setPhotoContext = (ctx, next) => {
	ctx.isPhoto = true;

	next();
};

// https://cs9.pikabu.ru/post_img/2017/08/02/8/1501677078121231378.gif
// http://s5.pikabu.ru/images/big_size_comm_an/2015-10_3/1444901735134491357.gif
// https://cs9.pikabu.ru/post_img/2017/08/02/8/1501677078121231378.mp4
// http://s5.pikabu.ru/images/big_size_comm_an/2015-10_3/1444901735134491357.mp4
// https://cs4.pikabu.ru/post_img/2014/11/14/11/1415990632_1414424441.gif
// https://cs9.pikabu.ru/post_img/2017/09/03/9/1504451718116836281.gif - without mp4
bot.hears(/((https?:\/\/c?s\d+\.pikabu\.ru\/(?:images\/big_size_comm_an)?(?:post_img)?\/(?:\d{4}-\d{1,2}_\d{1,2})?(?:\d{4}\/\d{2}\/\d{2}\/\d+)?)\/\d+(?:_\d+)?\.(gifv?|mp4))/, sendMessage);

// https://img-9gag-fun.9cache.com/photo/aMAj4Xx_460sv.mp4
// https://img-9gag-fun.9cache.com/photo/aMAj4Xx_460sv.gif
bot.hears(/(https?:\/\/img-9gag-fun\.\d+cache\.com\/photo\/.+\.(gifv?|mp4))/, sendMessage);

// https://giant.gfycat.com/WideUntriedBighornsheep.gif
// https://giant.gfycat.com/WideUntriedBighornsheep.mp4
bot.hears(/(https?:\/\/giant\.gfycat\.com\/.+\.(gifv?|mp4))/, sendMessage);

// http://i.imgur.com/PgXamVj.mp4
// http://i.imgur.com/PgXamVj.gif
bot.hears(/(https?:\/\/i\.imgur\.com\/.+\.(gifv?|mp4))/, imgurWrapper, sendMessage);

// https://i.redditmedia.com/d3gTUGoFe0-tDTklOiU9Tle8jgKfZIrDULDFyCezheo.jpg?s=bbb49633f5652e24b0be9f0113dee1f5
bot.hears(/(https?:\/\/i\.redditmedia\.com\/.+\.jpg(?:\?s=.+)?)/, setPhotoContext, sendMessage);

bot.hears(/(https?:\/\/video.twimg.com\/ext_tw_video\/\d+\/\w+\/\w+\/\d+x\d+\/\w+\.mp4)/, sendMessage);

bot.hears('@FreshodeModeratorBot', sendAngryMessage);

bot.catch(e => {
	console.error(e);
});

bot.startPolling();

console.log('Bot started');
